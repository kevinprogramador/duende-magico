$(window).bind('scroll', function() {
    var navHeight = $(window).height() - 100;
    if ($(window).scrollTop() > navHeight) {
        $('.navbar-inverse').addClass('on');
        //$('.quitarCrono').fadeOut();
    } else {
        $('.navbar-inverse').removeClass('on');
        //$('.quitarCrono').fadeIn();
    }
});

$(document).ready(function() {

    $('.popover.regresiva-old').click(function() {
        $('.popover.regresiva-old').fadeOut();
    });

    $('.navbar-brand').mouseenter(function() {
        $('.popover.regresiva-old').fadeIn();
    });

    $('.cerrarmenu').click(function() {
        $('#navbar').removeClass('in');
    });

    /* smooth scrolling sections */
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 0
            }, 1000);
            return false;
          }
        }
    });
});

if ($(window).width() < 550){
    $(document).ready(function() {
        $('.ocultardesktop').show();
        $('.ocultarmobile').hide();
    });
}else{
    $(document).ready(function() {
        $('.ocultardesktop').hide();
        $('.ocultarmobile').show();
    });
};

$(document).ready(function() {

    $('#clock').countdown('2019/12/25', function(event) {
        $('.clockDias').html(event.strftime('%D'));
        $('.clockHoras').html(event.strftime('%H : %M : %S'));
    });

    $('.box1').matchHeight();
    $('.boxModalProd').matchHeight();
    $('.boxDireccion').matchHeight();

    var sliderInicio = $('#sliderInicio');
    sliderInicio.owlCarousel({
      nav: false,
      dots: false,
      loop: true,
      items: 1,
      autoplay: true,
      autoplayHoverPause: true,
    });
    $('#sliderInicio-next').click(function() {
        sliderInicio.trigger('next.owl.carousel');
    });
    $('#sliderInicio-prev').click(function() {
        sliderInicio.trigger('prev.owl.carousel');
    });

    var  personajesSlider = $('#personajesSlider');
    personajesSlider.owlCarousel({
        rtl:true,
        loop: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: false,
        dots: false,
        //center:true,
        responsive:{
            0:{items:2,},
            500:{items:2,},
            700:{items:3,},
            950:{items:4,}
        }
    });
    $('#personajesSlider-next').click(function() {
        personajesSlider.trigger('next.owl.carousel');
    });
    $('#personajesSlider-prev').click(function() {
        personajesSlider.trigger('prev.owl.carousel');
    });

    var  productosSlider = $('#productosSlider');
    productosSlider.owlCarousel({
        loop: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: false,
        dots: false,
        //center:true,
        responsive:{
            0:{items:2,},
            500:{items:2,},
            700:{items:2,},
            950:{items:2,}
        }
    });
    $('#productosSlider-next').click(function() {
        productosSlider.trigger('next.owl.carousel');
    });
    $('#productosSlider-prev').click(function() {
        productosSlider.trigger('prev.owl.carousel');
    });

    var  modalProdSlider = $('.modalProdSlider');
    modalProdSlider.owlCarousel({
        loop: true,
        autoplay:true,
        autoplayHoverPause:false,
        nav: false,
        dots: true,
        lazyLoad:true,
        responsive:{
            0:{items:1,},
            500:{items:1,},
            700:{items:1,},
            950:{items:1,}
        }
    });

    var sliderHistoria = $('#sliderHistoria');
    sliderHistoria.owlCarousel({
      nav: false,
      dots: false,
      loop: true,
      items: 1,
      autoplay: false,
      callbacks: true,
      URLhashListener: true,
      //autoplayHoverPause: false,
      startPosition: 'URLHash'
    });

    $('#sliderHistoria-next').click(function() {
        sliderHistoria.trigger('next.owl.carousel');
    });
    $('#sliderHistoria-prev').click(function() {
        sliderHistoria.trigger('prev.owl.carousel');
    });

    var  historicosXyears = $('.historicosXyears');
    historicosXyears.owlCarousel({
        //loop: true,
        autoplay:false,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: true,
        navText:['<i class="fa fa-angle-double-left" aria-hidden="true"></i>','<i class="fa fa-angle-double-right" aria-hidden="true"></i>'],
        dots: false,
        //center:true,
        responsive:{
            0:{items:2,},
            500:{items:2,},
            700:{items:4,},
            950:{items:9,}
        }
    });
    $('#historicosXyears-next').click(function() {
        historicosXyears.trigger('next.owl.carousel');
    });
    $('#historicosXyears-prev').click(function() {
        historicosXyears.trigger('prev.owl.carousel');
    });

    var  juegosSlider = $('#juegosSlider');
    juegosSlider.owlCarousel({
        loop: true,
        autoplay:false,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: false,
        dots: false,

        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash',

        //center:true,
        responsive:{
            0:{items:1,},
            500:{items:1,},
            700:{items:1,},
            950:{items:1,}
        }
    });
    $('#juegosSlider-next').click(function() {
        juegosSlider.trigger('next.owl.carousel');
    });
    $('#juegosSlider-prev').click(function() {
        juegosSlider.trigger('prev.owl.carousel');
    });

    var  videosSlider = $('#videosSlider');
    videosSlider.owlCarousel({
        loop: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: false,
        dots: false,
        //center:true,
        responsive:{
            0:{items:2,},
            500:{items:2,},
            700:{items:2,},
            950:{items:2,}
        }
    });
    $('#videosSlider-next').click(function() {
        videosSlider.trigger('next.owl.carousel');
    });
    $('#videosSlider-prev').click(function() {
        videosSlider.trigger('prev.owl.carousel');
    });

    //

    //$('.owl-carousel .owl-item').on('mouseenter',function(e){
    //  $(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
    //})
    //$('.owl-carousel .owl-item').on('mouseleave',function(e){
    //  $(this).closest('.owl-carousel').trigger('play.owl.autoplay',[500]);
    //})

// PERSONAJES

    $('.p-Colorito').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorColorito");
        $("#personajeFoto").empty().append('<img src="img/duendes/colorito-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/colorito-title.png">');
        $("#personajeDescripcion").empty().append('El duende más travieso y apasionado de todos.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/G9muTLJq-ZI" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Es el duende más travieso y divertido de todos. Es simpático, alegre y creativo, pero también desordenado, un poco torpe y distraído. Amistoso y con gran corazón rojo. ');
    });
    $('.p-Celeste').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorCeleste");
        $("#personajeFoto").empty().append('<img src="img/duendes/celeste-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/celeste-title.png">');
        $("#personajeDescripcion").empty().append('Chisposa y entusiasta, alienta a sus amigos con el alma.').addClass('toFix').removeClass('blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/L0DH_8dkP4U" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Menta es una duende mágica simplemente fantástica. Su ingenio para todo saca aplausos donde quiera que vaya. Su mente es brillante y su sonrisa es refrescante. Todos se sienten atraídos por su creatividad');
    });
    $('.p-Nieve').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorNieve");
        $("#personajeFoto").empty().append('<img src="img/duendes/nieve-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/nieve-title.png">');
        $("#personajeDescripcion").empty().append('La destacada campeona lanzadora de bolas de nieve!').addClass('toFix blackTxt');
        $(".video-link a").addClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/L0DH_8dkP4U" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Es una inteligente y valiente duende mágica que sueña con desafiantes aventuras acuáticas, montada en un caballito de mar o un ágil delfín o nadando armoniosamente con una cola de sirena, descubriendo secretos, tesoros y sorpresas.');
    });
    $('.p-Amatista').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorAmatista");
        $("#personajeFoto").empty().append('<img src="img/duendes/amatista-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/amatista-title.png">');
        $("#personajeDescripcion").empty().append('La preciosa joyita mágica que sana penas, el alma y el corazón.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/L0DH_8dkP4U" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Menta es una duende mágica simplemente fantástica. Su ingenio para todo saca aplausos donde quiera que vaya. Su mente es brillante y su sonrisa es refrescante. Todos se sienten atraídos por su creatividad');
    });
    $('.p-Goldy').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorGoldy");
        $("#personajeFoto").empty().append('<img src="img/duendes/goldy-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/goldy-title.png">');
        $("#personajeDescripcion").empty().append('El astuto y perspicaz perro guardián de la magia dorada.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/SfsaMukxmrk" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Es una femenina duende mágica, tan dulce como un algodón de azúcar y su corazón es grande y esponjoso como un cupcake de frutillas.');
    });
    $('.p-Pistacho').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorPistacho");
        $("#personajeFoto").empty().append('<img src="img/duendes/pistacho-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/pistacho-title.png">');
        $("#personajeDescripcion").empty().append('El goleador del equipo boreal del Polo Norte.');
        $("#personajeDescripcion2").empty().append('El goleador del equipo boreal del Polo Norte.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/T6MaskihOyE" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
    });
    $('.p-Chocolata').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorChocolata");
        $("#personajeFoto").empty().append('<img src="img/duendes/chocolata-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/chocolata-title.png">');
        $("#personajeDescripcion").empty().append('La dulce perrita corazón de cacao.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/SfsaMukxmrk" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Su chispeante personalidad, dulzura y valentía conquistó a todos, especialmente a Goldy que quedó encantado con su gracioso movimiento de orejas.');
    });
    $('.p-BC').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorBC");
        $("#personajeFoto").empty().append('<img src="img/duendes/blanco-cherry-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/blanco-cherry-title.png">');
        $("#personajeDescripcion").empty().append('Los deliciosos y revoltosos cachorros de Chocolata y Goldy.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/KRLZrgdpzLk" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Es el guardián de la magia dorada.Lleva en el barril colgado en su cuello sus polvitos de magia para despertar los súper poderes que  cada niño tiene escondido en su corazón.');
    });
    $('.p-Zafiro').click(function() {
        $("#personajeColorBg").removeClass().addClass("bgColorZafiro");
        $("#personajeFoto").empty().append('<img src="img/duendes/zafiro-big.png" alt="" style="width:100%">');
        $("#personajeTitle").empty().append('<img src="img/duendes/zafiro-title.png">');
        $("#personajeDescripcion").empty().append('El inteligente reno que guiará el trineo taxi cargado de regalos.').removeClass('toFix blackTxt');
        $(".video-link a").removeClass('blackTxt');

        $("#addVideo").empty().append('<a href="https://youtu.be/tW-u4DFZa0E" target="_blank"><i class="fa fa-play-circle"></i> Ver Video</a>');
        //$("#personajeDescripcion2").empty().append('Es el guardián de la magia dorada.Lleva en el barril colgado en su cuello sus polvitos de magia para despertar los súper poderes que  cada niño tiene escondido en su corazón.');
    });


    $('.btnMultimedia').click(function() {
        $(".btnMultimedia").removeClass("altura100").addClass("altura30");
        $("#containerMultimedia").removeClass("altura0").addClass("altura70min");
    });

    $('#btnMulJuegos').click(function() {
        $("#contJuegos").slideDown();
        $("#contFotos, #contVideos").slideUp();
    });

    $('#btnMulFotos').click(function() {
        $("#contFotos").slideDown();
        $("#contJuegos, #contVideos").slideUp();
    });

    $('#btnMulVideos').click(function() {
        $("#contVideos").slideDown();
        $("#contFotos, #contJuegos").slideUp();
    });
});

//Animacion en Historicos
$(function() 
  {
      $(".questo").mouseenter(function(event) {
          $(this).addClass("animated swing");
      });
      $(".questo").on("webkitAnimationEnd mozAnimationEnd oAnimationEnd animationEnd", function(event) {
          $(this).removeClass("animated swing");
      });
  });

//ControlAudio
/*var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', 'audio/01_duendes.mp3');
    audioElement.setAttribute('autoplay', 'autoplay');
    audioElement.load()
    $.get();
    audioElement.addEventListener("load", function() {
      audioElement.play();
    }, true);
$('#play').hide();

$(".modal.videoYt").on('shown.bs.modal', function (e) {
    audioElement.pause();
});
$(".modal.videoYt").on('hidden.bs.modal', function (e) {
    audioElement.play();
});*/


//mute video BG
document.getElementById("player").volume = 0.0;
window.addEventListener("scroll", myFunction);
function myFunction() {
  var thetarget = document.getElementById("target");
  var targetpos = thetarget.offsetTop;
  var targetheight = thetarget.offsetHeight;
  var targetpostwo = targetpos + targetheight;
    if (document.body.scrollTop > targetpos && document.body.scrollTop < targetpostwo || document.documentElement.scrollTop > targetpos &&  document.documentElement.scrollTop < targetpostwo ) {
        document.getElementById("player").volume = 0.5;
    } else {
        document.getElementById("player").volume = 0.0;
    }
}

$(".modal.videoYt").on('hidden.bs.modal', function (e) {
    $(".modal.videoYt iframe").attr("src", $(".modal.videoYt iframe").attr("src"));
});
