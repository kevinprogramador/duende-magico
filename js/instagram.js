// How to get an access token:
// http://jelled.com/instagram/access-token
// {{model.user.username}}, {{likes}} likes

var galleryFeed = new Instafeed({
  get: "user",
  //userId: 5701569922,
  userId: 5701569922,
  //accessToken: "5701569922.1677ed0.58f4a4cde3244a39b0c74ebe4edd1176",
  accessToken: "5701569922.1677ed0.17ea83039dbb496e9d9983072f5354a2",
  resolution: "standard_resolution",
  useHttp: "true",
  limit: 30,
  template: 
    '<a href="{{link}}" target="_blank" title="ir a Instagram">'+
      '<div class="img-featured-container bgFoto altura70" style="background-image:url({{image}});">'+
        '<div class="img-backdrop"></div>'+
        '<div class="description-container">'+
          '<p class="caption">{{caption}}</p>'+
          '<span class="likes"><i class="fa fa-heart" aria-hidden="true"></i> {{likes}}</span>'+
          '<span class="comments"><i class="fa fa-comment" aria-hidden="true"></i> {{comments}}</span>'+
        '</div>'+
      '</div>'+
    '</a>',
  target: "instafeed-gallery-feed",
  after: function() {
    // disable button if no more results to load
    if (!this.hasNext()) {
      btnInstafeedLoad.setAttribute('disabled', 'disabled');
    }
    
    var owl = $(".owl-carousel"),
        owlSlideSpeed = 300;

    // init owl    
    $(document).ready(function(){
      owl.owlCarousel({
        // navContainer: '.owl-nav-custom',
        // dotsContainer: '.owl-dots-custom',
        loop:true,
        nav:false,
        dots: false,
        responsive:{
          0:{
            items:42
          },
          200:{
            items:2
          },
          400:{
            items:2
          },
          768:{
            items:2
          }
        }
      });
    });

    //var  videosSlider = $('#videosSlider');
    //videosSlider.owlCarousel({
    //    loop: true,
    //    autoplay:true,
    //    autoplayTimeout:4000,
    //    autoplayHoverPause:true,
    //    nav: false,
    //    dots: false,
    //    //center:true,
    //    responsive:{
    //        0:{items:2,},
    //        500:{items:2,},
    //        700:{items:2,},
    //        950:{items:2,}
    //    }
    //})
    $('#instagramSlider-next').click(function() {
        owl.trigger('next.owl.carousel');
    })
    $('#instagramSlider-prev').click(function() {
        owl.trigger('prev.owl.carousel');
    })

    // keyboard controls
    $(document.documentElement).keydown(function(event) {
      if (event.keyCode == 37) {
        owl.trigger('prev.owl.carousel', [owlSlideSpeed]);
      }
      else if (event.keyCode == 39) {
        owl.trigger('next.owl.carousel', [owlSlideSpeed]);
      }
    });
  }
});

galleryFeed.run();

var btnInstafeedLoad = document.getElementById("btn-instafeed-load");
btnInstafeedLoad.addEventListener("click", function() {
  galleryFeed.next()
});


